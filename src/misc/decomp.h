// SPDX-License-Identifier: 0BSD
#ifndef MISC_DECOMP
#define MISC_DECOMP

size_t decomp_pack_bits(unsigned char *restrict dst, const size_t dst_len,
const signed char *restrict src, const size_t src_len);

#endif /* MISC_DECOMP */
