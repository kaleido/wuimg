// SPDX-License-Identifier: 0BSD
#ifndef WU_EVENTS
#define WU_EVENTS

#include "window.h"

double event_exec(struct window_context *window);

void print_keys(void);

#endif /* WU_EVENTS */
