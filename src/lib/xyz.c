// SPDX-License-Identifier: 0BSD
#include <stdlib.h>

#include <zlib.h>

#include "raster/fmt.h"
#include "xyz.h"

size_t xyz_decode(const struct mparser *mp, struct wuimg *img) {
	/* XYZ is simply a deflate stream, containing an RGB palette and the
	 * index data.
	 * Although zlib allows us to pause decoding to switch output buffers,
	 * for simplicity, we decompress to a single buffer, expand the
	 * palette to RGBA in place, and make img->data point after
	 * img->u.palette while setting the borrowed bit to true. The image
	 * will be freed when the palette is. */
	struct mparser mpcpy = *mp;
	const struct wuptr src = mp_remaining(&mpcpy);
	if (src.len) {
		struct palette *pal;
		const size_t dst_len = sizeof(*pal) + wuimg_size(img);
		uint8_t *dst = malloc(dst_len);
		if (dst) {
			const size_t entries = 256;
			const size_t write_offset = sizeof(*pal) - entries*3;
			uint8_t *uncmp = dst + write_offset;
			uLong uncmp_len = (uLong)(dst_len - write_offset);
			uncompress(uncmp, &uncmp_len, src.ptr, (uLong)src.len);
			if (uncmp_len > entries*3) {
				pal = (struct palette *)dst;
				pal->refs = 0;
				palette_from_rgb8(pal, uncmp, entries);
				wuimg_palette_set(img, pal);
				img->data = dst + sizeof(*pal);
				img->borrowed = true;
				return uncmp_len;
			}
			free(dst);
		}
	}
	return 0;
}

enum wu_error xyz_parse(struct mparser *mp, struct wuimg *img) {
	const uint8_t *header = mp_slice(mp, 4);
	if (header) {
		img->w = buf_endian16(header, little_endian);
		img->h = buf_endian16(header + 2, little_endian);
		img->channels = 1;
		img->bitdepth = 8;
		img->layout = pix_rgba;
		return wuimg_verify(img);
	}
	return wu_unexpected_eof;
}

enum wu_error xyz_init(struct mparser *mp, const struct wuptr mem) {
	*mp = mp_wuptr(mem);
	const unsigned char magic[] = {'X', 'Y', 'Z', '1'};
	return fmt_sigcmp_mem(magic, sizeof(magic), mp);
}
