// SPDX-License-Identifier: 0BSD
#ifndef LIB_WBMP
#define LIB_WBMP

#include "raster/wuimg.h"

enum wu_error wbmp_open_file(struct wuimg *img, FILE *ifp);

#endif /* LIB_WBMP */
