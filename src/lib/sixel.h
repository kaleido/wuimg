// SPDX-License-Identifier: 0BSD
#ifndef LIB_SIXEL
#define LIB_SIXEL
#include "raster/wuimg.h"
#include "misc/mparser.h"

enum sixel_background_color {
	sixel_set_to_bg = 0,
	sixel_retain = 1,
};

struct sixel_desc {
	struct mparser tp;
	size_t data_end;

	enum sixel_background_color p2;
	unsigned char horizontal_grid_size;
};

size_t sixel_decode(const struct sixel_desc *desc, struct wuimg *img);

enum wu_error sixel_calc_parameters(struct sixel_desc *desc,
struct wuimg *img);

enum wu_error sixel_open_mem(struct sixel_desc *desc, struct wuptr mem);

#endif // LIB_SIXEL
