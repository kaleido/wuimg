// SPDX-License-Identifier: 0BSD
#include <string.h>

#include "misc/file.h"
#include "misc/math.h"
#include "misc/mem.h"
#include "raster/fmt.h"
#include "sun.h"

static size_t run_length_loop(unsigned char *restrict dst, const size_t dst_len,
const unsigned char *restrict rle, const size_t rle_len) {
	const unsigned char RLE_FLAG = 0x80;
	size_t d = 0;
	size_t r = 0;
	while (d < dst_len && r < rle_len) {
		if (rle[r] == RLE_FLAG) {
			++r;
			if (r >= rle_len) {
				break;
			}
			const unsigned char run_count = rle[r];
			++r;
			if (run_count) {
				if (dst_len - d < (size_t)run_count + 1
				|| r >= rle_len) {
					return d;
				}
				memset(dst + d, rle[r], run_count + 1);
				d += run_count + 1;
				++r;
			} else {
				dst[d] = RLE_FLAG;
				++d;
			}
		} else {
			const size_t read = memccpy_cur(dst + d, rle + r, RLE_FLAG,
				dst_len - d, rle_len - r);
			d += read;
			r += read;
		}
	}
	return d;
}

static size_t rle_decode(const struct sun_desc *desc,
unsigned char *restrict dst, const size_t dst_len) {
	// E.g. 0x80 0x00 0x80 0x00... -> 0x80 0x80...
	const size_t pathological_rle = dst_len * 2;
	const size_t file_size = file_remaining(desc->ifp);

	size_t written = 0;
	const size_t rle_len = zumin(file_size, pathological_rle);
	unsigned char *rle = malloc(rle_len);
	if (rle) {
		written = run_length_loop(dst, dst_len, rle,
			fread(rle, 1, rle_len, desc->ifp));
		free(rle);
	}
	return written;
}

size_t sun_decode(const struct sun_desc *desc, struct wuimg *img) {
	if (wuimg_alloc_noverify(img)) {
		if (desc->type == sun_byte_encoded) {
			return rle_decode(desc, img->data, wuimg_size(img));
		}
		return fmt_load_raster(img, desc->ifp);
	}
	return 0;
}

static enum wu_error interleave_colormap(struct sun_desc *desc,
struct wuimg *img) {
	uint8_t buf[256*3];
	if (fread(buf, sizeof(buf), 1, desc->ifp)) {
		struct palette *map = wuimg_palette_init(img);
		if (map) {
			const size_t entries = 1 << img->bitdepth;
			for (size_t i = 0; i < entries; ++i) {
				map->color[i] = (struct pix_rgba8) {
					.r = buf[i],
					.g = buf[i + entries],
					.b = buf[i + entries*2],
					.a = 0xff,
				};
			}
			return wu_ok;
		}
		return wu_alloc_error;
	}
	return wu_unexpected_eof;
}

static enum wu_error validate_header(struct sun_desc *desc, struct wuimg *img,
const uint32_t width, const uint32_t height, const uint32_t bitdepth,
const uint32_t type, const uint32_t cm_type, const uint32_t cm_len) {
	switch (type) {
	case sun_old:
	case sun_standard:
	case sun_byte_encoded:
	case sun_rgb:
		break;
	case sun_tiff:
	case sun_iff:
	case sun_experimental:
		return wu_samples_wanted;
	default:
		return wu_invalid_header;
	}

	switch (cm_type) {
	case sun_no_colormap:
		if (cm_len) {
			return wu_invalid_header;
		}
		break;
	case sun_rgb_colormap:
		if (bitdepth > 8 || cm_len != (1U << bitdepth) * 3) {
			return wu_invalid_header;
		}
		break;
	case sun_raw_colormap:
		return wu_samples_wanted;
	default:
		return wu_invalid_header;
	}

	switch (bitdepth) {
	case 1: case 4:
		img->attr = cm_type ? pix_normal : pix_inverted;
		break;
	case 8:
		break;
	case 24:
		img->layout = (type == sun_rgb) ? pix_rgba : pix_bgra;
		break;
	case 32:
		img->layout = (type == sun_rgb) ? pix_argb : pix_abgr;
		break;
	default:
		return wu_invalid_header;
	}

	img->w = width;
	img->h = height;
	img->channels = (uint8_t)umax(bitdepth/8, 1);
	img->bitdepth = (uint8_t)umin(bitdepth, 8);
	img->align_sh = 1;

	desc->type = type;
	desc->colormap_type = cm_type;
	if (cm_type != sun_no_colormap) {
		return interleave_colormap(desc, img);
	}
	return wu_ok;
}

enum wu_error sun_parse_header(struct sun_desc *desc, struct wuimg *img) {
	/* SUN header (after magic bytes)
		Offset  Size    Name
		0       DWORD   Width;
		4       DWORD   Height;
		8       DWORD   Depth;          // Bits per pixel[1]
		12      DWORD   Length;         // Size of image data.
		16      DWORD   Type;           // Type of raster file
		20      DWORD   ColorMapType;
		24      DWORD   ColorMapLen;    // In bytes
		28      VAR     ColorMap;       // Planar RGB
		??
	*/

	uint32_t header[7];
	if (!fread(header, sizeof(header), 1, desc->ifp)) {
		return wu_unexpected_eof;
	}

	enum wu_error st = validate_header(desc, img,
		endian32(header[0], big_endian),
		endian32(header[1], big_endian),
		endian32(header[2], big_endian),
		endian32(header[4], big_endian),
		endian32(header[5], big_endian),
		endian32(header[6], big_endian));
	if (st == wu_ok) {
		st = wuimg_verify(img);
	}
	return st;
}

enum wu_error sun_open_file(struct sun_desc *desc, FILE *ifp) {
	desc->ifp = ifp;
	const unsigned char sig[] = {0x59, 0xa6, 0x6a, 0x95};
	return fmt_sigcmp(sig, sizeof(sig), ifp);
}
