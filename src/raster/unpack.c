// SPDX-License-Identifier: 0BSD
#include <string.h>
#include <limits.h>

#include "misc/bit.h"
#include "misc/endian.h"
#include "raster/strip.h"
#include "raster/unpack.h"

// Expand 2^n-bit to 8-bit
static void expand4(const uint_fast8_t byte, uint8_t *dst, const size_t nr,
const uint8_t s) {
	switch (nr) {
	case 2:
		dst[1] = (uint8_t)( (byte & 0x0f) * s );
		// fallthrough
	case 1:
		dst[0] = (uint8_t)( (byte >> 4) * s );
	}
}

static void expand2(const uint_fast8_t byte, uint8_t *dst, const size_t nr,
const uint8_t s) {
	switch (nr) {
	case 4:
		dst[3] = (uint8_t)( (byte & 0x03) * s );
		// fallthrough
	case 3:
		dst[2] = (uint8_t)( ((byte >> 2) & 0x03) * s );
		// fallthrough
	case 2:
		dst[1] = (uint8_t)( ((byte >> 4) & 0x03) * s );
		// fallthrough
	case 1:
		dst[0] = (uint8_t)( (byte >> 6) * s );
		// fallthrough
	}
}

static void expand1(const uint_fast8_t byte, uint8_t *dst, const size_t nr,
const uint8_t s) {
	for (size_t i = 0; i < nr; ++i) {
		dst[i] = (byte & (0x80 >> i)) ? s : 0x00;
	}
}

// Have the compiler inline one of the above
static inline void select_unpack(const uint_fast8_t byte, uint8_t *dst,
const size_t nr, const enum unpack_op action, const size_t bitdepth) {
	switch (action) {
	case op_unpack:
		switch (bitdepth) {
		case 1: expand1(byte, dst, nr, 1); break;
		case 2: expand2(byte, dst, nr, 1); break;
		case 4: expand4(byte, dst, nr, 1); break;
		}
		break;
	case op_expand:
		switch (bitdepth) {
		case 1: expand1(byte, dst, nr, 0xff); break;
		case 2: expand2(byte, dst, nr, 0xff / 0x03); break;
		case 4: expand4(byte, dst, nr, 0xff / 0x0f); break;
		}
		break;
	default:
		break;
	}
}

// The above but looping
static inline void strip_common(uint8_t *restrict dst,
const uint8_t *restrict src, const size_t width, const uint8_t bitdepth,
const enum unpack_op op, const uint8_t xor) {
	const size_t ppb = 8 / bitdepth;

	const size_t bytes = width / ppb;
	const size_t remainer = width % ppb;
	for (size_t x = 0; x < bytes; ++x) {
		const size_t o = x * ppb;
		const uint8_t byte = src[x] ^ xor;
		select_unpack(byte, dst + o, ppb, op, bitdepth);
	}
	if (remainer) {
		const uint8_t byte = src[bytes] ^ xor;
		select_unpack(byte, dst + width - remainer, remainer, op,
			bitdepth);
	}
}

static void strip_xor4(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n, const enum pix_attr attr) {
	const uint8_t xor = (attr == pix_signed) ? 0x88 : 0xff;
	strip_common(dst, src, n, 4, op_expand, xor);
}

static void strip_xor2(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n, const enum pix_attr attr) {
	const uint8_t xor = (attr == pix_signed) ? 0xaa : 0xff;
	strip_common(dst, src, n, 2, op_expand, xor);
}

static void strip_xor1(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 1, op_expand, 0xff);
}

static void strip_expand4(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 4, op_expand, 0);
}

static void strip_expand2(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 2, op_expand, 0);
}

static void strip_expand1(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 1, op_expand, 0);
}

static void strip_unpack4(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 4, op_unpack, 0);
}

static void strip_unpack2(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 2, op_unpack, 0);
}

static void strip_unpack1(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 1, op_unpack, 0);
}

static void strip_unpack_xor1(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n) {
	strip_common(dst, src, n, 1, op_unpack, 0xff);
}

static void strip_invert(uint8_t *restrict dst, const uint8_t *restrict src,
const size_t n, const uint8_t bytedepth, const enum pix_attr attr) {
	if (attr == pix_inverted) {
		for (size_t x = 0; x < n; ++x) {
			dst[x] = src[x] ^ 0xff;
		}
	} else { // pix_signed
		const size_t step = (which_end() == little_endian)
			? bytedepth - 1 : 0;
		for (size_t x = 0; x < n; x += bytedepth) {
			for (size_t i = 0; i < bytedepth; ++i) {
				const uint8_t c = src[x+i];
				if (i == step) {
					dst[x+i] = c ^ 0x80;
				} else {
					dst[x+i] = c;
				}
			}
		}
	}
}

// Unpack any bitdepth < 16 and non-power-of-two
static inline void strip_small_common(void *restrict dst,
const void *restrict src, const size_t width, const uint8_t bitdepth,
const enum pix_attr attr, const enum unpack_op op) {
	const uint8_t outdepth = (bitdepth > 8) ? 16 : 8;

	const uint32_t inrange = (1u << bitdepth) - 1;
	const uint32_t outrange = (1u << outdepth) - 1;
	const uint32_t scale = (outrange << 16) / inrange + 1;
	uint32_t xor;
	switch (attr) {
	case pix_signed: xor = (1u << (bitdepth - 1)); break;
	case pix_inverted: xor = inrange; break;
	default: xor = 0; break;
	}
	for (size_t x = 0; x < width; ++x) {
		uint32_t pix = bit_getn(src, x*bitdepth, bitdepth) ^ xor;
		if (op == op_expand) {
			pix = (pix * scale) >> 16;
		}
		switch (outdepth) {
		case 8: ((uint8_t *)dst)[x] = (uint8_t)pix; break;
		case 16: ((uint16_t *)dst)[x] = (uint16_t)pix; break;
		}
	}
}

static void strip_sm_unpack(void *restrict dst, const void *restrict src,
const size_t n, const uint8_t bitdepth, const enum pix_attr attr) {
	strip_small_common(dst, src, n, bitdepth, attr, op_unpack);
}

static void strip_sm_expand(void *restrict dst, const void *restrict src,
const size_t n, const uint8_t bitdepth, const enum pix_attr attr) {
	strip_small_common(dst, src, n, bitdepth, attr, op_expand);
}


// Pack any bitdepth > 16
static inline uint16_t select_wordpack(const void *src, const size_t x,
const uint8_t bitdepth) {
	switch (bitdepth) {
	case 32: return (uint16_t)( ((const uint32_t *)src)[x] >> (32 - 16) );
	case 64: return (uint16_t)( ((const uint64_t *)src)[x] >> (64 - 16) );
	}
	return (uint16_t)bit_getn(src, x*bitdepth, 16);
}

static inline void strip_wordpack_common(uint16_t *dst, const void *restrict src,
const size_t width, const uint8_t bitdepth, const enum pix_attr attr) {
	uint16_t xor;
	switch (attr) {
	case pix_signed: xor = 0x8000; break;
	case pix_inverted: xor = 0xffff; break;
	default: xor = 0; break;
	}
	for (size_t x = 0; x < width; ++x) {
		dst[x] = select_wordpack(src, x, bitdepth) ^ xor;
	}
}

static void strip_pack32_16(uint16_t *restrict dst, const void *restrict src,
const size_t n, const enum pix_attr attr) {
	strip_wordpack_common(dst, src, n, 32, attr);
}

static void strip_pack64_16(uint16_t *restrict dst, const void *restrict src,
const size_t n, const enum pix_attr attr) {
	strip_wordpack_common(dst, src, n, 64, attr);
}

static void strip_pack_16(uint16_t *restrict dst, const void *restrict src,
const size_t n, const uint8_t bitdepth, const enum pix_attr attr) {
	strip_wordpack_common(dst, src, n, bitdepth, attr);
}

static void strip_pack64f_32f(float *dst, const double *src, const size_t len) {
	for (size_t i = 0; i < len; ++i) {
		dst[i] = (float)(src[i]);
	}
}


void unpack_strip(void *restrict dst, const void *restrict src,
const size_t n, const uint8_t bitdepth, const enum pix_attr attr,
const enum unpack_op op, const void *arg) {
	switch (op) {
	case op_noop:
		memcpy(dst, src, strip_base(n, bitdepth));
		break;
	case op_unpack:
		switch (attr) {
		case pix_normal:
			switch (bitdepth) {
			case 1: strip_unpack1(dst, src, n); return;
			case 2: strip_unpack2(dst, src, n); return;
			case 4: strip_unpack4(dst, src, n); return;
			}
			break;
		case pix_inverted:
		case pix_signed:
			switch (bitdepth) {
			case 1: strip_unpack_xor1(dst, src, n); return;
			default:
				if (bitdepth % 8 == 0) {
					strip_invert(dst, src, n, bitdepth/8, attr);
					return;
				}
				break;
			}
			break;
		default: return;
		}
		strip_sm_unpack(dst, src, n, bitdepth, attr);
		break;
	case op_expand:
		switch (attr) {
		case pix_normal:
			switch (bitdepth) {
			case 1: strip_expand1(dst, src, n); break;
			case 2: strip_expand2(dst, src, n); break;
			case 4: strip_expand4(dst, src, n); break;
			default: strip_sm_expand(dst, src, n, bitdepth, attr);
			}
			break;
		case pix_signed:
		case pix_inverted:
			switch (bitdepth) {
			case 1: strip_xor1(dst, src, n); break;
			case 2: strip_xor2(dst, src, n, attr); break;
			case 4: strip_xor4(dst, src, n, attr); break;
			default:
				if (bitdepth % 8) {
					strip_sm_expand(dst, src, n, bitdepth, attr);
				} else {
					strip_invert(dst, src, n, bitdepth/8, attr);
				}
				break;
			}
			break;
		case pix_float:
			break;
		}
		break;
	case op_pack:
		switch (attr) {
		case pix_normal:
		case pix_signed:
		case pix_inverted:
			switch (bitdepth) {
			case 32: strip_pack32_16(dst, src, n, attr); break;
			case 64: strip_pack64_16(dst, src, n, attr); break;
			default: strip_pack_16(dst, src, n, bitdepth, attr);
			}
			break;
		case pix_float:
			if (bitdepth == 64) {
				strip_pack64f_32f(dst, src, n);
			}
			break;
		}
		break;
	case op_remap:
		;const struct remap_info *nfo = arg;
		remap_scale(dst, src, n, *nfo);
		break;
	case op_bitfield:
		bitfield_unpack(arg, dst, src, n);
		break;
	}
}

static uint8_t get_unpackdepth(const uint8_t depth) {
	if (depth && depth < 16) {
		return (depth > 8) ? 16 : 8;
	}
	return 0;
}

static uint8_t unpack_depth(const uint8_t bitdepth, const enum pix_attr attr,
const enum unpack_op op, const void *arg) {
	switch (op) {
	case op_noop:
		return bitdepth;
	case op_unpack:
	case op_expand:
		switch (attr) {
		case pix_normal:
		case pix_signed:
		case pix_inverted:
			if (bitdepth % 8) {
				return get_unpackdepth(bitdepth);
			}
			return bitdepth;
		case pix_float:
			break;
		}
		break;
	case op_pack:
		switch (attr) {
		case pix_normal:
		case pix_signed:
		case pix_inverted:
			if (bitdepth > 16) {
				return 16;
			}
			break;
		case pix_float:
			if (bitdepth == 64) {
				return 32;
			}
			break;
		}
		break;
	case op_remap:
		;const struct remap_info *info = arg;
		return (uint8_t)(8 << info->depth_sh);
	case op_bitfield:
		;const struct bitfield *bf = arg;
		return bf->outdepth * bf->ch;
	}
	return 0;
}

size_t unpack_stride(const size_t n, const uint8_t bitdepth,
const enum pix_attr attr, const enum unpack_op op, const void *arg) {
	uint8_t outdepth = unpack_depth(bitdepth, attr, op, arg);
	if (outdepth) {
		return strip_base(n, outdepth);
	}
	return 0;
}


// Convert signed to unsigned and scale
static uint64_t ams(int32_t c, uint32_t add, uint64_t mul, uint8_t shr) {
	return (((uint32_t)c + add) * mul) >> shr;
}
static void design8(uint8_t *dst, const int8_t *src, const size_t w,
const uint64_t mul, const uint32_t add) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint8_t)ams(src[x], (uint8_t)add, (uint16_t)mul, 8);
	}
}
static void design16(uint16_t *dst, const int16_t *src, const size_t w,
const uint64_t mul, const uint32_t add) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint16_t)ams(src[x], (uint16_t)add, (uint32_t)mul, 16);
	}
}
static void design32(uint32_t *dst, const int32_t *src, const size_t w,
const uint64_t mul, const uint32_t add) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint32_t)ams(src[x], add, mul, 32);
	}
}

// Scale
static uint64_t msx(uint32_t c, uint64_t mul, uint8_t shr, uint32_t xor) {
	return ((c * mul) >> shr) ^ xor;
}
static void scale8(uint8_t *dst, const uint8_t *src, const size_t w,
const uint64_t mul, const uint32_t xor) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint8_t)msx(src[x], (uint16_t)mul, 8, xor);
	}
}
static void scale16(uint16_t *dst, const uint16_t *src, const size_t w,
const uint64_t mul, const uint32_t xor) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint16_t)msx(src[x], (uint32_t)mul, 16, xor);
	}
}
static void scale32(uint32_t *dst, const uint32_t *src, const size_t w,
const uint64_t mul, const uint32_t xor) {
	for (size_t x = 0; x < w; ++x) {
		dst[x] = (uint32_t)msx(src[x], mul, 32, xor);
	}
}

static void exact_mul(uint32_t *dst, const uint32_t *src, const size_t w,
const uint32_t mul, const uint8_t depth) {
	const size_t div = 2 - depth;
	const size_t items = w >> div;
	const size_t remain = w - (items << div);
	for (size_t x = 0; x < items; ++x) {
		dst[x] = src[x] * mul;
	}
	if (remain) {
		uint32_t tmp = 0;
		memcpy(&tmp, src, remain);
		tmp *= mul;
		memcpy(dst + items, &tmp, remain);
	}
}

void remap_scale(void *dst, const void *src, const size_t n,
const struct remap_info info) {
	const uint8_t depth = info.depth_sh;
	const uint64_t mul = info.mul;
	const uint32_t var = info.var;
	if (info.scale) {
		switch (info.attr) {
		case pix_signed:
			switch (depth) {
			case 0: design8(dst, src, n, mul, var); break;
			case 1: design16(dst, src, n, mul, var); break;
			case 2: design32(dst, src, n, mul, var); break;
			}
			return;
		case pix_normal:
			if (info.exact) {
				exact_mul(dst, src, n, var, depth);
				return;
			}
			// fallthrough
		case pix_inverted:
			switch (depth) {
			case 0: scale8(dst, src, n, mul, var); break;
			case 1: scale16(dst, src, n, mul, var); break;
			case 2: scale32(dst, src, n, mul, var); break;
			}
		case pix_float:
			break;
		}
	} else {
		switch (info.attr) {
		case pix_normal:
			if (dst != src) {
				memcpy(dst, src, n << depth);
			}
			break;
		case pix_signed:
		case pix_inverted:
			strip_invert(dst, src, n, (uint8_t)(8 << depth),
				info.attr);
			break;
		case pix_float:
			break;
		}
	}
}

struct remap_info remap_scale_info(const uint32_t maxval,
const uint8_t outdepth, const enum pix_attr attr) {
	const uint32_t range = bit_set32(outdepth);
	struct remap_info info = (struct remap_info) {
		.scale = range != maxval,
		.exact = range % maxval == 0 && attr == pix_normal,
		.depth_sh = bit_min_wordsize_log2(outdepth),
		.attr = attr,
	};
	switch (attr) {
	case pix_inverted:
		info.var = ~(uint32_t)0;
		break;
	case pix_signed:
		info.var = maxval/2 + 1;
		break;
	case pix_normal: case pix_float:
		break;
	}

	if (info.exact) {
		info.var = range / maxval;
	} else {
		info.mul = ((uint64_t)range << outdepth) / maxval + 1;
	}
	return info;
}
