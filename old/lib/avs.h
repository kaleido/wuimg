// SPDX-License-Identifier: 0BSD
#ifndef LIB_AVS
#define LIB_AVS

#include "raster/wuimg.h"

enum wu_error avs_open_file(struct wuimg *img, FILE *ifp);

#endif /* LIB_AVS */
