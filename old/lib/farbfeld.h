// SPDX-License-Identifier: 0BSD
#ifndef LIB_FARBFELD
#define LIB_FARBFELD

#include "raster/wuimg.h"

enum wu_error farbfeld_open_file(struct wuimg *img, FILE *ifp);

#endif /* LIB_FARBFELD */
